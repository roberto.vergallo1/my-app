import React, {useState, useEffect} from 'react';

function Toggle() {

    const [toggle, setToggle] = useState();

    return (
        <>
            <p>Il valore del toggle è {toggle}</p>
            <button onClick={() => setToggle('On')}>On</button>
            <button onClick={() => setToggle('Off')}>Off</button>
        </>
    );
}


export default Toggle;