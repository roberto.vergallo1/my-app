import React, {useState, useEffect} from 'react';

export default function Login() {

    const [formState, setFormState] = useState({});
    const [apigClient, setApigClient] = useState(null);

    useEffect(() => {
        if(window.apigClientFactory) {
            const client = window.apigClientFactory.newClient();
            setApigClient(client);
        } else {
            console.error('apigClientFactory non è definito');
        }
    }, []);

    const handleChange = ({target}) => {
        const {name, value} = target;
        setFormState((prev) => ({
            ...prev, [name]:value
        }));
    }

    const loginFunction = () => {
        console.log('username: '+formState.username);
        console.log('password: '+formState.password);

        var params = {};
        var body = {
            email: formState.username,
            password: formState.password
        };
        var additionalParams = {};
        
        apigClient.apiV1UsersLoginPost(params, body, additionalParams)
            .then(function(result){
                //This is where you would put a success callback
                //console.log(JSON.stringify(result, '', 2));
                const jwt = result.data.jwt;
                localStorage.setItem('token', jwt);
                console.log('Il token è: '+localStorage.getItem('token'));
            }).catch( function(result){
                //This is where you would put an error callback
                console.log(JSON.stringify(result, '', 2));
            });
    }

    return (
        <>
            <form>
                Username: <input type='text' name='username' onChange={handleChange} value={formState.username || ''}/><br/>
                Password: <input type='password' name='password' onChange={handleChange} value={formState.password || ''}/><br/>
            </form>
            <button onClick={loginFunction}>Login</button>
        </>
    );

}