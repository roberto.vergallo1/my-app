import React, {useState} from 'react'

function EmailInput() {

    const [email, setEmail] = useState('');

    const emailHandler = (event) => {
        const updatedEmail = event.target.value;
        console.log(updatedEmail);
        setEmail(updatedEmail)
    };

    return (
        <>
            EMAIL: <input value={email} type='text' onChange={emailHandler} />
        </>
    );

}

export default EmailInput;