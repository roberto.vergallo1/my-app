import {useNavigate} from 'react-router-dom';

const Home = () => {

    const navigate = useNavigate();

    const gotoBlog =() => {
        navigate('/blogs');
    }

    return (
        <>
            <h1>Home</h1>
            <button onClick={gotoBlog}>vai a blog</button>
        </>
    );

}

export default Home;