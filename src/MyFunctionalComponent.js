import React, {useState, useEffect} from 'react';

const MyFunctionalComponent = ({updateMessage, message}) => {

    const [n, setN] = useState(1);
    //let n = Math.floor(Math.random() * 10 + 1);

    console.log("Il valore generato è "+n);

    useEffect(() => {
        setN(Math.floor(Math.random() * 10 + 1));
    }, []);

    return (
            <>
                <hr width='50%'/>
                    <HeadLine number={n}/>
                    <button onClick={() => setN(7)}>Click me</button>
            </>
        );
}

const HeadLine = ({number}) => <h1>Ciao sono un functional component! {number}</h1>;

export default MyFunctionalComponent;