import React from 'react';

const MyFunctionalComponentConProps = (props) => {

    console.log(props);

    const n = Math.floor(Math.random() * 10 + 1);

    console.log("Il valore generato è "+n);

    return (
            <>
                <hr width='50%'/>
                    <HeadLine number={n}/>
                    <button onClick={() => props.updateMessage(props.message)}>Click me</button>
            </>
        );
}

const HeadLine = ({number}) => <h1>Ciao sono un functional component! {number}</h1>;

export default MyFunctionalComponent;