import React from 'react';


class MyComponentClass extends React.Component {

    constructor(props) {
        super(props);
        this.state = {mood: 'decent'};
        this.myFunc = this.myFunc.bind(this);
    }

    get food() {
        return 'Ice cream';
    }

    myFunc() {
        //alert('Cliccato');
        //this.state.mood = 'happy';
        this.setState({mood: 'happy'});
    }

    componentDidMount() {
        console.log('Mi trovo dopo il render');
    }

    render() {

        const n = Math.floor(Math.random() * 10 + 1);

        const pic = {
            title: 'titolo',
            src: 'https://picsum.photos/300/200?rand='+Math.random()
        };

        console.log(this.props.children);

        console.log('Fine del render');

        return (
                <>
                    <h1>Hello world {n} {this.food} {this.state.mood}</h1>
                    <h2>Token: {localStorage.getItem('token')}</h2>
                    <img src={pic.src} title={pic.title} onClick={this.myFunc}/>
                    {this.props.children}
                </>
        );
    }
}

export default MyComponentClass;