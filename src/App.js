import logo from './logo.svg';
import './App.css';
import './MyComponentClass';
import MyComponentClass from './MyComponentClass';
import MySecondComponentClass from './MySecondComponentClass';
import MyFunctionalComponent from './MyFunctionalComponent';
import Toggle from './ToggleExample';
import EmailInput from './EmailInputExample';
import Counter from './CounterExample';
import ArrayHome from './ArrayHome';
import Login from './Login';

function alertFunction(text) {
    alert(text);
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      <MyComponentClass/>
      <MyComponentClass>
        <h3>testo inserito in MyComponentClass come children</h3>
      </MyComponentClass>
      <MySecondComponentClass name='Roberto' />
      <MyFunctionalComponent updateMessage={alertFunction} message='Testo passato da app.js' />

      <Toggle />
      <EmailInput/>
      <Counter />
      <br/>
      <ArrayHome />
      <Login />
      <br/><br/><br/><br/><br/>
      </header>
    </div>
  );
}

export default App;
