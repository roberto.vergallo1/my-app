import React, {useState} from 'react';

function ArrayHome() {

    const staticItems = ['item1', 'item2', 'item3', 'item4', 'item5'];

    const [items, setItems] = useState([]);

    const clickHandler = ({target}) => {
        setItems(prev => [target.value, ...prev]);
    }

    return (
        <>
            {staticItems.map((item) => <button onClick={clickHandler} key={item} value={item}>{item}</button>)}
            <ul>
                {
                    items.map((item, index) => (
                        <li key={index}>{item}</li>
                    ))}
            </ul>
        </>
    );

}

export default ArrayHome;