import React from 'react';
import MyComponentClass from './MyComponentClass';

class MySecondComponentClass extends React.Component {

    handleEvent() {
        alert('Hai cliccato qualcosa nel second component');
    }

    render() {
        console.log(this.props);
        return (
                    <>
                        <hr width='50%'/>
                        Ciao {this.props.name}, sono il secondo componente 
                        <MyComponentClass/>
                        <button onClick={this.handleEvent}>Pulsante in second component</button>
                    </>
                );
    }
}

export default MySecondComponentClass;