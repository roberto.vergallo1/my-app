import React, {useState} from 'react';

function Counter() {

    const [count, setCount] = useState(0);

    const increment = () => setCount(prevCount => prevCount+1);

    return (
        <>
            <p>Wow, hai cliccato {count} volte!!!</p>
            <button onClick={increment}>Cliccami</button>
        </>
    );

}

export default Counter;